# downloads and temporary output directory
$(shell mkdir -p $(target_out))

# Check buildroot
# download folder
filesystem_path := $(shell ls $(sdk_dir) 2>/dev/null)
ifeq ($(strip $(filesystem_path)),)
$(info *** Link download dir ***)
$(info $(shell ln -s $(root_dir)/../sdk $(sdk_dir)))
endif

# extract buildroot
filesystem_path := $(shell ls $(target_out_buildroot) 2>/dev/null)
ifeq ($(strip $(filesystem_path)),)
$(info *** Extract busybox source ***)
$(info $(shell tar -xvf $(sdk_dir)/${buildroot_version}.tar.gz -C $(target_out)))
endif


filesystem_path := $(shell ls $(target_out_buildroot) 2>/dev/null)
ifneq ($(strip $(filesystem_path)),)
  # buildroot download folder
  filesystem_path := $(shell ls $(target_out_buildroot)/dl 2>/dev/null)
  ifeq ($(strip $(filesystem_path)),)
  $(info *** Link buildroot download dir ***)
  $(info $(shell ln -s $(sdk_dir)/buildroot_dl $(target_out_buildroot)/dl))
  endif
  
  # buildroot config
  filesystem_path := $(shell ls $(target_out_buildroot)/.config 2>/dev/null)
  ifeq ($(strip $(filesystem_path)),)
  $(info *** set buildroot config to default ***)
  $(info $(shell cp configs/buildroot_config $(target_out_buildroot)/.config))
  endif

  # overlay dir
  filesystem_path := $(shell ls $(target_out_buildroot)/output/overlaydir 2>/dev/null)
  ifeq ($(strip $(filesystem_path)),)
  $(info *** copy overlay dir ***)
  $(info $(shell mkdir -p $(target_out_buildroot)/output))
  $(info $(shell cp -rf configs/overlaydir $(target_out_buildroot)/output/))
  endif
endif

# linux config
filesystem_path := $(shell ls $(target_out_kernel) 2>/dev/null)
ifneq ($(strip $(filesystem_path)),)
  filesystem_path := $(shell ls $(target_out_kernel)/kernel_config 2>/dev/null)
  ifeq ($(strip $(filesystem_path)),)
  $(info *** set kernel config to default ***)
  $(info $(shell cp configs/kernel_config $(target_out_kernel)/.config))
  $(info $(shell cp configs/kernel_config $(target_out_kernel)/))
  $(info $(shell cp configs/suniv-f1c100s-licheepi-nano.dts $(target_out_kernel)/arch/arm/boot/dts/))
  $(info $(shell cp configs/suniv.dtsi $(target_out_kernel)/arch/arm/boot/dts/))
  endif
endif

# busybox config
filesystem_path := $(shell ls $(target_out_busybox)/.config 2>/dev/null)
ifneq ($(strip $(filesystem_path)),)
  filesystem_path := $(shell ls $(target_out_busybox)/busybox_config 2>/dev/null)
  ifeq ($(strip $(filesystem_path)),)
  $(info *** set busybox config to default ***)
  $(info $(shell cp configs/busybox_config $(target_out_busybox)/.config))
  $(info $(shell cp configs/busybox_config $(target_out_busybox)/))
  endif  
endif
