build-uboot:
	$(shell mkdir -p ${target_out_uboot})
	env LANG=C make -C $(uboot_dir) \
		ARCH=arm CROSS_COMPILE=$(CROSS_COMPILE) \
		O=$(target_out_uboot) \
		licheepi_nano_spiflash_defconfig
	$(shell mkdir -p ${target_out_uboot})
	env LANG=C make -C $(uboot_dir) \
		ARCH=arm CROSS_COMPILE=$(CROSS_COMPILE) \
		O=$(target_out_uboot) -j`nproc`
        
uboot-menuconfig:
	$(shell mkdir -p ${target_out_uboot})
	env LANG=C make -C $(uboot_dir) \
		ARCH=arm CROSS_COMPILE=$(CROSS_COMPILE) \
		O=$(target_out_uboot) \
		menuconfig
