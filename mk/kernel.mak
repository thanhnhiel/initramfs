build-kernel:
	$(shell mkdir -p ${target_out_kernel})
	cp -f configs/kernel_config $(target_out)/kernel/.config
	cp -f configs/suniv-f1c100s-licheepi-nano.dts $(kernel_dir)/arch/arm/boot/dts/
	cp -f configs/suniv.dtsi $(kernel_dir)/arch/arm/boot/dts/
	make -C $(kernel_dir) \
		ARCH=arm CROSS_COMPILE=$(CROSS_COMPILE) \
		O=$(target_out_kernel) \
		zImage modules suniv-f1c100s-licheepi-nano.dtb -j`nproc`
	cp $(target_out)/kernel/arch/arm/boot/zImage $(target_out)/
	cp $(target_out)/kernel/arch/arm/boot/dts/suniv-f1c100s-licheepi-nano.dtb $(target_out)/

linux-menuconfig:
	$(shell mkdir -p ${target_out_kernel})
	cp -f configs/kernel_config $(target_out)/kernel/.config
	make -C $(kernel_dir) \
		ARCH=arm CROSS_COMPILE=$(CROSS_COMPILE) \
		O=$(target_out_kernel) \
		menuconfig