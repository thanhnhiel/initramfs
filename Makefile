# Filesystem images  ---> [*] initial RAM filesystem linked into linux kernel  

root_dir := $(shell pwd)
parent_dir := $(realpath $(root_dir)/../../)
sdk_dir := $(realpath $(HOME)/logger/sdk)

buildroot_version := buildroot-2017.08
kernel_version := linux-4.19.0-rc3
busybox_version := busybox-1.27.1

target_out := $(root_dir)/output
target_out_buildroot := $(target_out)/$(buildroot_version)
target_out_kernel := $(target_out_buildroot)/output/build/$(kernel_version)
target_out_busybox := $(target_out_buildroot)/output/build/$(busybox_version)
 
.PHONY: all prepare linux-menuconfig busybox-menuconfig
all: prepare 

prepare:

include mk/download.mak

$(boot_script):
	mkimage -C none -A arm -T script -d configs/boot.cmd $(boot_script)

menuconfig:
	make -C $(target_out_buildroot) $@

linux-menuconfig:
	make -C $(target_out_buildroot) $@

busybox-menuconfig:
	make -C $(target_out_buildroot) $@

all:
	make -C $(target_out_buildroot)

rootfs:
	-rm $(target_out_buildroot)/output/target/lib/*.so | true
	-rm $(target_out_buildroot)/output/target/lib/*.so.* | true
	find $(target_out_buildroot)/output/target/lib/ -type f -name "*.ko" -exec rm -f {} \; | true
	-rm $(target_out_buildroot)/output/target/etc/init.d/S10udev | true
	-rm $(target_out_buildroot)/output/target/etc/udev | true
	-rm $(target_out_buildroot)/output/target/etc/network | true
	make -C $(target_out_buildroot) rootfs-cpio
    
initramfs:
	-rm $(target_out_buildroot)/output/images/zImage | true
	make -C $(target_out_buildroot) 